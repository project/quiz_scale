<?php

/**
 * @file
 * Handles the layout of the scale answering form.
 *
 * Variables available:
 * - $form.
 */
print \Drupal::service('renderer')->render($form);
